<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
<style>
	body{
		display:flex;
		align-items:center;
		flex-direction:column;
	}
</style>
</head>
	<body>
		<h1>Welcome <%=session.getAttribute("fullName")%>! </h1>
		<%
			if(session.getAttribute("userType").equals("applicant")){
				out.println("<p>Welcome "+session.getAttribute("userType")+". You may now start looking for you career opportunity.</p>");
			}else if(session.getAttribute("userType").equals("employer")){
				out.println("<p>Welcome "+session.getAttribute("userType")+". You may now start browsing applicant profiles.</p>");
			}
		%>
	</body>
</html>