<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmation</title>
<style>
	div{
		display:flex;
		flex-direction: column;
	}

	#container{
		display:flex;
		align-items:center;
		flex-direction: column;
	}
	
	form{
		margin:10px;
	}
</style>
</head>
	<body>
		<%
			String userType = session.getAttribute("userType").toString();
			userType = userType.substring(0, 1).toUpperCase() + userType.substring(1);
			
			String referral = session.getAttribute("referral").toString();
			if(referral.equals("friends")){
				referral = "Friends";
			}else if(referral.equals("others")){
				referral = "Others";
			}else{
				referral = "Social Media";
			}
		%>
		<div id="container">
			<h1>Registration confirmation</h1>
			<div>
				<p>First Name: <%= session.getAttribute("firstName") %></p>
				<p>Last Name: <%= session.getAttribute("lastName") %></p>
				<p>Phone: <%= session.getAttribute("phone") %></p>
				<p>Email: <%= session.getAttribute("email") %></p>
				<p>App Discovery: <%= referral %></p>
				<p>Date of Birth: <%= session.getAttribute("birthdate") %></p>
				<p>User Type: <%= userType %></p>
				<p>Description: <%= session.getAttribute("description") %></p>
				
				<form action="login" method="post">
					<input type="submit">
				</form>
				
				<form action="index.jsp">
					<input type="submit" value="Back">
				</form>
			</div>
		</div>
	</body>
</html>