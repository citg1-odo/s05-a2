<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Job Finder</title>
<style>
	div{
		margin:10px;
	}

	#container{
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		margin-left:10px;
		margin-right:10px;
	}
	
	div>input, div>select, div>textarea{
		width: 100%;
	}
</style>
</head>
	<body>
		<div id="container">
			<h1>Welcome to Servlet Job Finder!</h1>
			<form action="register" method="post">
				<div>
					<label for="firstName">First Name</label>
					<input name="firstName" type="text" required>
				</div>
				<div>
					<label for="lastName">Last Name</label>
					<input style={width:100px} name="lastName" type="text" required>
				</div>
				<div>
					<label  for="phone">Phone</label>
					<input name="phone" type="tel" required>
				</div>
				<div>
					<label  for="email">Email</label>
					<input name="email" type="email" required>
				</div>
			
				<fieldset>
					<legend>How did you discover the app?</legend>
					
					<input type="radio" id="friends" name="referral" value="friends"/>
					<label for="friends">Friends</label>
					<br>
					<input type="radio" id="social_media" name="referral" value="socialMedia"/>
					<label for="social_media">Social Media</label>
					<br>
					<input type="radio" id="others" name="referral" value="others"/>
					<label for="others">Others</label>
				</fieldset>
				<div>
				<label  for="birthdate">Date of birth</label>
				<input name="birthdate" type="date" required>
				</div>
				<div>
				<label for="userType">Are you an employer or an applicant?</label>
				<select name="userType" required>
					<option value="" selected>Select One</option>
					<option value="applicant" >Applicant</option>
					<option value="employer" >Employer</option>
				</select>
				</div>
				<div>
				<label for="description">Profile Description</label>
				<textarea name="description" maxlength="500"></textarea>
				</div>
				<div>
				<button type="submit">Register</button>
				</div>
			</form>
		</div>
	</body>
</html>