package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;

	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String referral = req.getParameter("referral");
		String birthdate = req.getParameter("birthdate");
		String userType = req.getParameter("userType");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("referral", referral);
		session.setAttribute("birthdate", birthdate);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized");
	}
}
